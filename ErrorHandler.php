<?php
class ErrorHandler {

    private $logger;

    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    public function handle(Exception $exception) {
        var_dump('*********** Catch Exception: We write to the logger ***********', $exception->getMessage());
    }
}