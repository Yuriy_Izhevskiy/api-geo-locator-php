<?php
include_once('./IpGeoLocationLocator.php');
include_once('./IpApiLocator.php');
include_once('./ChainLocator.php');
include_once ('./Ip.php');
include_once ('./HttpClient.php');
include_once ('./MuteLocator.php');
include_once ('./ErrorHandler.php');
include_once ('./Logger.php');
include_once ('./CacheLocator.php');
include_once ('./Cache.php');

$handler = new ErrorHandler(new Logger());
$client = new HttpClient();
$cache = new Cache();

$locator = new ChainLocator(
    new CacheLocator(
        new MuteLocator(
            new IpGeoLocationLocator($client, '74101a8cb3124920921fbbe3a650ae59'),
            $handler
        ),
        $cache,
        'cache_1',
        3600
    ),
    new CacheLocator(
        new MuteLocator(
            new IpApiLocator($client, ''),
            $handler
        ),
        $cache,
        'cache_1',
        3600
    )
);


$res = $locator->locate(new Ip('178.165.0.148'));
var_dump($res->getCountry());