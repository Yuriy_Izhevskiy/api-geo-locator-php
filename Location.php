<?php
class Location {
    private $country;
    private $region;
    private $city;

    public function __construct(string $country, ?string $region, ?string $city) {
        $this->country = $country;
        $this->region = $region;
        $this->city = $city;
    }

    public function getCountry(): string {
        return $this->country;
    }

    public function getRegion(): ?string {
        return $this->region;
    }

    public function getCity(): ?string {
        return $this->city;
    }

    public function getPrecision(): int {
        if (empty($this->city)) {
            return 3;
        }
        if (empty($this->region)) {
            return 2;
        }
        if (empty($this->country)) {
            return 1;
        }
        return 0;
    }
}