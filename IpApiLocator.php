<?php
include_once ('./Location.php');
include_once ('./interfaceLocator.php');

class IpApiLocator implements Locator {
    private $client;
    private $apiKey;

    public function __construct(HttpClient $client, string $apiKey) {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    public function locate(Ip $ip): ?Location {

        $url = 'http://ip-api.com/json/' . $ip->getValue();

        $response = $this->client->get($url);
        $data = json_decode($response, true);

        $data = array_map(function($value) { return $value !== '-' ? $value : null; }, $data);

        if (empty($data['country'])) {
            return null;
        }
        return new Location(
            $data['country'],
            $data['zip'],
            $data['city']);
    }
}