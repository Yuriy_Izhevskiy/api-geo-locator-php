<?php
include_once ('./interfaceLocator.php');

class CacheLocator implements Locator {
    private $next;
    private $cache;
    private $ttl;
    private $prefix;

    public function __construct(Locator $next, Cache $cache, string $prefix, int $ttl) {
        $this->next = $next;
        $this->cache = $cache;
        $this->ttl = $ttl;
        $this->prefix = $prefix;
    }

    public function locate(Ip $ip): ?Location {
        $key = $this->prefix .'-location-' . $ip->getValue();
        $location = $this->cache->get($key);

        if ($location === null) {
            $location = $this->next->locate($ip);
            $this->cache->set($key, $location, $this->ttl);
        }

        return $location;
    }
}